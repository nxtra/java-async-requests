
import lombok.AllArgsConstructor;
import org.asynchttpclient.*;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class CallingAsync {

    private static final Logger LOGGER = Logger.getLogger(CallingAsync.class.getName());
    private static final String URL = "https://api.chucknorris.io/jokes/random";
    private static final int MYTHREADS = 10;
    private static ExecutorService executor = Executors.newFixedThreadPool(MYTHREADS);

    public static void main(String args[]) {
        long startTime = System.currentTimeMillis();
        LOGGER.info("Starting, timer started");
        DefaultAsyncHttpClientConfig.Builder clientBuilder = Dsl.config()
                .setConnectTimeout(5000)
                .setMaxConnections(10);
        AsyncHttpClient client = Dsl.asyncHttpClient(clientBuilder);


        for (int i = 0; i < 30; i++) {
            LOGGER.info("CallingAsync Norris time : " + i);
            Runnable worker = new MyRunnable(URL, client, i);
            LOGGER.info("Executing : " + i);
            executor.execute(worker);
        }
        executor.shutdown();
        while (!executor.isTerminated()) {

        }
        LOGGER.info("\nFinished all threads");

        long endTime = System.currentTimeMillis();
        System.out.println("Finished in " + (endTime - startTime) + " milliseconds");

        System.exit(0);
    }

    @AllArgsConstructor
    public static class MyRunnable implements Runnable {

        private final String url;
        private final AsyncHttpClient client;
        private int number;

        public void run() {

            try {
                long startTime = System.currentTimeMillis();
                BoundRequestBuilder getRequest = client.prepareGet(url);

                Response response = (Response) getRequest.execute(new AsyncCompletionHandler<Object>() {

                    @Override
                    public Object onCompleted(Response response) {
                        return response;
                    }
                }).get();
                long endTime = System.currentTimeMillis();
                System.out.println("CallNumber: " + number + " in " + (endTime - startTime) + " milliseconds"
                        + "\nExecuted by thread: " + Thread.currentThread().getName() + "\nResponse: " + response.getResponseBody());

            } catch (Exception e) {
                LOGGER.info(e.getMessage());
            }
        }
    }
}