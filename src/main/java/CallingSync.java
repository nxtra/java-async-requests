import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class CallingSync {
    OkHttpClient client = new OkHttpClient();

    String run(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

    public static void main(String[] args) throws IOException {
        long startTime = System.currentTimeMillis();

        CallingSync example = new CallingSync();

        for(int i = 0; i < 30; i++) {
            String response = example.run("https://api.chucknorris.io/jokes/random");
            System.out.println(response);
        }

        long endTime = System.currentTimeMillis();
        System.out.println("Finished in " + (endTime - startTime) + " milliseconds");
    }
}

